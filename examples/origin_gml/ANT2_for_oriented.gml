graph [
	directed 1
	node [
		id 1
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Actinomycetales;f__Corynebacteriaceae;g__Corynebacterium;s__durum"
		mv 0
	]
	node [
		id 2
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Actinomycetales;f__Micrococcaceae;g__Rothia;s__mucilaginosa"
		mv 0
	]
	node [
		id 3
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__adolescentis"
		mv 0
	]
	node [
		id 4
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__animalis"
		mv 0
	]
	node [
		id 5
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__bifidum"
		mv 0
	]
	node [
		id 6
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__breve"
		mv 0
	]
	node [
		id 7
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__longum"
		mv 0
	]
	node [
		id 8
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__pseudolongum"
		mv 0
	]
	node [
		id 9
		label "Root;k__Bacteria;p__Actinobacteria;c__Coriobacteriia;o__Coriobacteriales;f__Coriobacteriaceae;g__Collinsella;s__aerofaciens"
		mv 0
	]
	node [
		id 10
		label "Root;k__Bacteria;p__Actinobacteria;c__Coriobacteriia;o__Coriobacteriales;f__Coriobacteriaceae;g__Eggerthella;s__lenta"
		mv 0
	]
	node [
		id 11
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__acidifaciens"
		mv 0
	]
	node [
		id 12
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__barnesiae"
		mv 0
	]
	node [
		id 13
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__caccae"
		mv 0
	]
	node [
		id 14
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__coprophilus"
		mv 0
	]
	node [
		id 15
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__eggerthii"
		mv 0
	]
	node [
		id 16
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__fragilis"
		mv 0
	]
	node [
		id 17
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__ovatus"
		mv 0
	]
	node [
		id 18
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__plebeius"
		mv 0
	]
	node [
		id 19
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__uniformis"
		mv 0
	]
	node [
		id 20
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Porphyromonadaceae;g__Parabacteroides;s__distasonis"
		mv 0
	]
	node [
		id 21
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Porphyromonadaceae;g__Parabacteroides;s__gordonii"
		mv 0
	]
	node [
		id 22
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Prevotellaceae;g__Prevotella;s__copri"
		mv 0
	]
	node [
		id 23
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Prevotellaceae;g__Prevotella;s__stercorea"
		mv 0
	]
	node [
		id 24
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Rikenellaceae;g__Alistipes;s__indistinctus"
		mv 0
	]
	node [
		id 25
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Rikenellaceae;g__Alistipes;s__massiliensis"
		mv 0
	]
	node [
		id 26
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Enterococcaceae;g__Enterococcus;s__casseliflavus"
		mv 0
	]
	node [
		id 27
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Lactobacillaceae;g__Lactobacillus;s__brevis"
		mv 0
	]
	node [
		id 28
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Lactobacillaceae;g__Lactobacillus;s__iners"
		mv 0
	]
	node [
		id 29
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Lactobacillaceae;g__Lactobacillus;s__zeae"
		mv 0
	]
	node [
		id 30
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Streptococcaceae;g__Lactococcus;s__garvieae"
		mv 0
	]
	node [
		id 31
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Streptococcaceae;g__Streptococcus;s__anginosus"
		mv 0
	]
	node [
		id 32
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__botulinum"
		mv 0
	]
	node [
		id 33
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__neonatale"
		mv 0
	]
	node [
		id 34
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__perfringens"
		mv 0
	]
	node [
		id 35
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__tyrobutyricum"
		mv 0
	]
	node [
		id 36
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Blautia;s__obeum"
		mv 0
	]
	node [
		id 37
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Blautia;s__producta"
		mv 0
	]
	node [
		id 38
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Coprococcus;s__catus"
		mv 0
	]
	node [
		id 39
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Coprococcus;s__eutactus"
		mv 0
	]
	node [
		id 40
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Dorea;s__formicigenerans"
		mv 0
	]
	node [
		id 41
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Roseburia;s__faecis"
		mv 0
	]
	node [
		id 42
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__[Ruminococcus];s__gnavus"
		mv 0
	]
	node [
		id 43
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__[Ruminococcus];s__torques"
		mv 0
	]
	node [
		id 44
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Peptostreptococcaceae;g__Peptostreptococcus;s__anaerobius"
		mv 0
	]
	node [
		id 45
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Peptostreptococcaceae;g__[Clostridioides];s__difficile"
		mv 0
	]
	node [
		id 46
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Butyricicoccus;s__pullicaecorum"
		mv 0
	]
	node [
		id 47
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Clostridium;s__hungatei"
		mv 0
	]
	node [
		id 48
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Clostridium;s__methylpentosum"
		mv 0
	]
	node [
		id 49
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Faecalibacterium;s__prausnitzii"
		mv 0
	]
	node [
		id 50
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Ruminococcus;s__bromii"
		mv 0
	]
	node [
		id 51
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Ruminococcus;s__callidus"
		mv 0
	]
	node [
		id 52
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Ruminococcus;s__flavefaciens"
		mv 0
	]
	node [
		id 53
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Veillonellaceae;g__Veillonella;s__dispar"
		mv 0
	]
	node [
		id 54
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Veillonellaceae;g__Veillonella;s__parvula"
		mv 0
	]
	node [
		id 55
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Bulleidia;s__moorei"
		mv 0
	]
	node [
		id 56
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Clostridium;s__saccharogumia"
		mv 0
	]
	node [
		id 57
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Coprobacillus;s__cateniformis"
		mv 0
	]
	node [
		id 58
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__[Eubacterium];s__biforme"
		mv 0
	]
	node [
		id 59
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__[Eubacterium];s__dolichum"
		mv 0
	]
	node [
		id 60
		label "Root;k__Bacteria;p__Lentisphaerae;c__[Lentisphaeria];o__Victivallales;f__Victivallaceae;g__Victivallis;s__vadensis"
		mv 0
	]
	node [
		id 61
		label "Root;k__Bacteria;p__Proteobacteria;c__Alphaproteobacteria;o__Rickettsiales;f__mitochondria;g__Nelumbo;s__nucifera"
		mv 0
	]
	node [
		id 62
		label "Root;k__Bacteria;p__Proteobacteria;c__Betaproteobacteria;o__Burkholderiales;f__Comamonadaceae;g__Xylophilus;s__ampelinus"
		mv 0
	]
	node [
		id 63
		label "Root;k__Bacteria;p__Proteobacteria;c__Betaproteobacteria;o__Burkholderiales;f__Oxalobacteraceae;g__Janthinobacterium;s__lividum"
		mv 0
	]
	node [
		id 64
		label "Root;k__Bacteria;p__Proteobacteria;c__Betaproteobacteria;o__Burkholderiales;f__Oxalobacteraceae;g__Oxalobacter;s__formigenes"
		mv 0
	]
	node [
		id 65
		label "Root;k__Bacteria;p__Proteobacteria;c__Deltaproteobacteria;o__Desulfovibrionales;f__Desulfovibrionaceae;g__Desulfovibrio;s__D168"
		mv 0
	]
	node [
		id 66
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Alteromonadales;f__Shewanellaceae;g__Shewanella;s__algae"
		mv 0
	]
	node [
		id 67
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Cronobacter;s__dublinensis"
		mv 0
	]
	node [
		id 68
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Enterobacter;s__arachidis"
		mv 0
	]
	node [
		id 69
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Enterobacter;s__cloacae"
		mv 0
	]
	node [
		id 70
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Enterobacter;s__ludwigii"
		mv 0
	]
	node [
		id 71
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Klebsiella;s__oxytoca"
		mv 0
	]
	node [
		id 72
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Pantoea;s__agglomerans"
		mv 0
	]
	node [
		id 73
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Serratia;s__marcescens"
		mv 0
	]
	node [
		id 74
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Oceanospirillales;f__Halomonadaceae;g__Halomonas;s__salifodinae"
		mv 0
	]
	node [
		id 75
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Actinobacillus;s__parahaemolyticus"
		mv 0
	]
	node [
		id 76
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Aggregatibacter;s__pneumotropica"
		mv 0
	]
	node [
		id 77
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Aggregatibacter;s__segnis"
		mv 0
	]
	node [
		id 78
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Haemophilus;s__parainfluenzae"
		mv 0
	]
	node [
		id 79
		label "Root;k__Bacteria;p__Verrucomicrobia;c__Verrucomicrobiae;o__Verrucomicrobiales;f__Verrucomicrobiaceae;g__Akkermansia;s__muciniphila"
		mv 0
	]
	edge [
		source 3
		target 7
		weight 0.9171833992004395
	]
	edge [
		source 3
		target 8
		weight 0.8104436993598938
	]
	edge [
		source 9
		target 10
		weight -0.6075204610824585
	]
	edge [
		source 15
		target 19
		weight 0.7704209685325623
	]
	edge [
		source 19
		target 20
		weight 0.5986986756324768
	]
	edge [
		source 21
		target 27
		weight 0.0
	]
	edge [
		source 21
		target 30
		weight 0.964699923992157
	]
	edge [
		source 28
		target 33
		weight 0.8023986220359802
	]
	edge [
		source 25
		target 34
		weight 0.48971062898635864
	]
	edge [
		source 14
		target 38
		weight 0.7478341460227966
	]
	edge [
		source 31
		target 47
		weight 0.683335542678833
	]
	edge [
		source 51
		target 56
		weight 0.6616541147232056
	]
	edge [
		source 49
		target 57
		weight -0.6238808631896973
	]
	edge [
		source 55
		target 57
		weight -0.6572545766830444
	]
	edge [
		source 10
		target 59
		weight 0.7001368999481201
	]
	edge [
		source 43
		target 59
		weight 0.5895150303840637
	]
	edge [
		source 2
		target 62
		weight -0.6926742196083069
	]
	edge [
		source 34
		target 62
		weight -0.8442040681838989
	]
	edge [
		source 25
		target 65
		weight 0.5750885009765625
	]
	edge [
		source 12
		target 67
		weight 0.7451633214950562
	]
	edge [
		source 47
		target 68
		weight 0.8623748421669006
	]
	edge [
		source 67
		target 68
		weight 0.9987883567810059
	]
	edge [
		source 63
		target 69
		weight 0.9999999403953552
	]
	edge [
		source 70
		target 71
		weight 0.0
	]
	edge [
		source 68
		target 72
		weight 0.9997825622558594
	]
	edge [
		source 12
		target 73
		weight -0.999245285987854
	]
	edge [
		source 63
		target 74
		weight 0.0
	]
	edge [
		source 54
		target 75
		weight 0.5854678750038147
	]
	edge [
		source 26
		target 77
		weight -0.8919458389282227
	]
	edge [
		source 45
		target 77
		weight 0.5333902835845947
	]
	edge [
		source 75
		target 78
		weight 0.7749343514442444
	]
]
