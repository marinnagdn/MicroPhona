graph [
	directed 0
	node [
		id 1
		label "Root;k__Archaea;p__Euryarchaeota;c__Methanobacteria;o__Methanobacteriales;f__Methanobacteriaceae;g__Methanobrevibacter;s__arboriphilus"
		mv 0
	]
	node [
		id 2
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Actinomycetales;f__Corynebacteriaceae;g__Corynebacterium;s__durum"
		mv 0
	]
	node [
		id 3
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Actinomycetales;f__Micrococcaceae;g__Rothia;s__aeria"
		mv 0
	]
	node [
		id 4
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Actinomycetales;f__Micrococcaceae;g__Rothia;s__dentocariosa"
		mv 0
	]
	node [
		id 5
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Actinomycetales;f__Micrococcaceae;g__Rothia;s__mucilaginosa"
		mv 0
	]
	node [
		id 6
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__adolescentis"
		mv 0
	]
	node [
		id 7
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__animalis"
		mv 0
	]
	node [
		id 8
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__bifidum"
		mv 0
	]
	node [
		id 9
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__breve"
		mv 0
	]
	node [
		id 10
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__longum"
		mv 0
	]
	node [
		id 11
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__pseudolongum"
		mv 0
	]
	node [
		id 12
		label "Root;k__Bacteria;p__Actinobacteria;c__Coriobacteriia;o__Coriobacteriales;f__Coriobacteriaceae;g__Collinsella;s__aerofaciens"
		mv 0
	]
	node [
		id 13
		label "Root;k__Bacteria;p__Actinobacteria;c__Coriobacteriia;o__Coriobacteriales;f__Coriobacteriaceae;g__Eggerthella;s__lenta"
		mv 0
	]
	node [
		id 14
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__acidifaciens"
		mv 0
	]
	node [
		id 15
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__barnesiae"
		mv 0
	]
	node [
		id 16
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__caccae"
		mv 0
	]
	node [
		id 17
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__coprophilus"
		mv 0
	]
	node [
		id 18
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__eggerthii"
		mv 0
	]
	node [
		id 19
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__fragilis"
		mv 0
	]
	node [
		id 20
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__ovatus"
		mv 0
	]
	node [
		id 21
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__plebeius"
		mv 0
	]
	node [
		id 22
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__uniformis"
		mv 0
	]
	node [
		id 23
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Porphyromonadaceae;g__Parabacteroides;s__distasonis"
		mv 0
	]
	node [
		id 24
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Porphyromonadaceae;g__Parabacteroides;s__gordonii"
		mv 0
	]
	node [
		id 25
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Prevotellaceae;g__Prevotella;s__copri"
		mv 0
	]
	node [
		id 26
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Prevotellaceae;g__Prevotella;s__melaninogenica"
		mv 0
	]
	node [
		id 27
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Prevotellaceae;g__Prevotella;s__stercorea"
		mv 0
	]
	node [
		id 28
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Rikenellaceae;g__Alistipes;s__indistinctus"
		mv 0
	]
	node [
		id 29
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Rikenellaceae;g__Alistipes;s__massiliensis"
		mv 0
	]
	node [
		id 30
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Enterococcaceae;g__Enterococcus;s__casseliflavus"
		mv 0
	]
	node [
		id 31
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Lactobacillaceae;g__Lactobacillus;s__brevis"
		mv 0
	]
	node [
		id 32
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Lactobacillaceae;g__Lactobacillus;s__zeae"
		mv 0
	]
	node [
		id 33
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Streptococcaceae;g__Lactococcus;s__garvieae"
		mv 0
	]
	node [
		id 34
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Streptococcaceae;g__Streptococcus;s__anginosus"
		mv 0
	]
	node [
		id 35
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__botulinum"
		mv 0
	]
	node [
		id 36
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__butyricum"
		mv 0
	]
	node [
		id 37
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__neonatale"
		mv 0
	]
	node [
		id 38
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__perfringens"
		mv 0
	]
	node [
		id 39
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__tyrobutyricum"
		mv 0
	]
	node [
		id 40
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Blautia;s__obeum"
		mv 0
	]
	node [
		id 41
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Blautia;s__producta"
		mv 0
	]
	node [
		id 42
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Clostridium;s__hathewayi"
		mv 0
	]
	node [
		id 43
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Coprococcus;s__eutactus"
		mv 0
	]
	node [
		id 44
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Dorea;s__formicigenerans"
		mv 0
	]
	node [
		id 45
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Robinsoniella;s__peoriensis"
		mv 0
	]
	node [
		id 46
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Roseburia;s__faecis"
		mv 0
	]
	node [
		id 47
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__[Ruminococcus];s__gnavus"
		mv 0
	]
	node [
		id 48
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__[Ruminococcus];s__torques"
		mv 0
	]
	node [
		id 49
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Peptostreptococcaceae;g__Clostridium;s__maritimum"
		mv 0
	]
	node [
		id 50
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Peptostreptococcaceae;g__Peptostreptococcus;s__anaerobius"
		mv 0
	]
	node [
		id 51
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Peptostreptococcaceae;g__[Clostridioides];s__difficile"
		mv 0
	]
	node [
		id 52
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Butyricicoccus;s__pullicaecorum"
		mv 0
	]
	node [
		id 53
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Clostridium;s__hungatei"
		mv 0
	]
	node [
		id 54
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Clostridium;s__methylpentosum"
		mv 0
	]
	node [
		id 55
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Faecalibacterium;s__prausnitzii"
		mv 0
	]
	node [
		id 56
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Ruminococcus;s__bromii"
		mv 0
	]
	node [
		id 57
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Ruminococcus;s__callidus"
		mv 0
	]
	node [
		id 58
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Ruminococcus;s__flavefaciens"
		mv 0
	]
	node [
		id 59
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Veillonellaceae;g__Veillonella;s__dispar"
		mv 0
	]
	node [
		id 60
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Veillonellaceae;g__Veillonella;s__parvula"
		mv 0
	]
	node [
		id 61
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Bulleidia;s__moorei"
		mv 0
	]
	node [
		id 62
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Bulleidia;s__p-1630-c5"
		mv 0
	]
	node [
		id 63
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Clostridium;s__cocleatum"
		mv 0
	]
	node [
		id 64
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Clostridium;s__saccharogumia"
		mv 0
	]
	node [
		id 65
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Coprobacillus;s__cateniformis"
		mv 0
	]
	node [
		id 66
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__[Eubacterium];s__biforme"
		mv 0
	]
	node [
		id 67
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__[Eubacterium];s__dolichum"
		mv 0
	]
	node [
		id 68
		label "Root;k__Bacteria;p__Lentisphaerae;c__[Lentisphaeria];o__Victivallales;f__Victivallaceae;g__Victivallis;s__vadensis"
		mv 0
	]
	node [
		id 69
		label "Root;k__Bacteria;p__Proteobacteria;c__Alphaproteobacteria;o__Rickettsiales;f__mitochondria;g__Nelumbo;s__nucifera"
		mv 0
	]
	node [
		id 70
		label "Root;k__Bacteria;p__Proteobacteria;c__Alphaproteobacteria;o__Rickettsiales;f__mitochondria;g__Spirodela;s__polyrhiza"
		mv 0
	]
	node [
		id 71
		label "Root;k__Bacteria;p__Proteobacteria;c__Betaproteobacteria;o__Burkholderiales;f__Comamonadaceae;g__Variovorax;s__paradoxus"
		mv 0
	]
	node [
		id 72
		label "Root;k__Bacteria;p__Proteobacteria;c__Betaproteobacteria;o__Burkholderiales;f__Comamonadaceae;g__Xylophilus;s__ampelinus"
		mv 0
	]
	node [
		id 73
		label "Root;k__Bacteria;p__Proteobacteria;c__Betaproteobacteria;o__Burkholderiales;f__Oxalobacteraceae;g__Oxalobacter;s__formigenes"
		mv 0
	]
	node [
		id 74
		label "Root;k__Bacteria;p__Proteobacteria;c__Deltaproteobacteria;o__Desulfovibrionales;f__Desulfovibrionaceae;g__Desulfovibrio;s__D168"
		mv 0
	]
	node [
		id 75
		label "Root;k__Bacteria;p__Proteobacteria;c__Epsilonproteobacteria;o__Campylobacterales;f__Campylobacteraceae;g__Campylobacter;s__rectus"
		mv 0
	]
	node [
		id 76
		label "Root;k__Bacteria;p__Proteobacteria;c__Epsilonproteobacteria;o__Campylobacterales;f__Campylobacteraceae;g__Campylobacter;s__ureolyticus"
		mv 0
	]
	node [
		id 77
		label "Root;k__Bacteria;p__Proteobacteria;c__Epsilonproteobacteria;o__Campylobacterales;f__Helicobacteraceae;g__Helicobacter;s__pullorum"
		mv 0
	]
	node [
		id 78
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Alteromonadales;f__Shewanellaceae;g__Shewanella;s__algae"
		mv 0
	]
	node [
		id 79
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Citrobacter;s__freundii"
		mv 0
	]
	node [
		id 80
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Cronobacter;s__dublinensis"
		mv 0
	]
	node [
		id 81
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Cronobacter;s__turicensis"
		mv 0
	]
	node [
		id 82
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Enterobacter;s__arachidis"
		mv 0
	]
	node [
		id 83
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Enterobacter;s__ludwigii"
		mv 0
	]
	node [
		id 84
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Klebsiella;s__oxytoca"
		mv 0
	]
	node [
		id 85
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Serratia;s__marcescens"
		mv 0
	]
	node [
		id 86
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Oceanospirillales;f__Halomonadaceae;g__Halomonas;s__pacifica"
		mv 0
	]
	node [
		id 87
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Actinobacillus;s__parahaemolyticus"
		mv 0
	]
	node [
		id 88
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Aggregatibacter;s__pneumotropica"
		mv 0
	]
	node [
		id 89
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Aggregatibacter;s__segnis"
		mv 0
	]
	node [
		id 90
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Haemophilus;s__parainfluenzae"
		mv 0
	]
	node [
		id 91
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Pasteurella;s__multocida"
		mv 0
	]
	node [
		id 92
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pseudomonadales;f__Moraxellaceae;g__Acinetobacter;s__johnsonii"
		mv 0
	]
	node [
		id 93
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pseudomonadales;f__Moraxellaceae;g__Acinetobacter;s__lwoffii"
		mv 0
	]
	node [
		id 94
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Xanthomonadales;f__Xanthomonadaceae;g__Stenotrophomonas;s__geniculata"
		mv 0
	]
	node [
		id 95
		label "Root;k__Bacteria;p__Synergistetes;c__Synergistia;o__Synergistales;f__Dethiosulfovibrionaceae;g__Pyramidobacter;s__piscolens"
		mv 0
	]
	node [
		id 96
		label "Root;k__Bacteria;p__Verrucomicrobia;c__Verrucomicrobiae;o__Verrucomicrobiales;f__Verrucomicrobiaceae;g__Akkermansia;s__muciniphila"
		mv 0
	]
	edge [
		source 2
		target 3
		weight 0.7938833832740784
	]
	edge [
		source 3
		target 4
		weight 1.0
	]
	edge [
		source 6
		target 10
		weight 0.8627000451087952
	]
	edge [
		source 6
		target 11
		weight 0.752757728099823
	]
	edge [
		source 18
		target 22
		weight 0.6997380256652832
	]
	edge [
		source 20
		target 22
		weight 0.4938451051712036
	]
	edge [
		source 15
		target 24
		weight 0.49582186341285706
	]
	edge [
		source 18
		target 25
		weight -0.6547901630401611
	]
	edge [
		source 26
		target 27
		weight 0.5849533081054688
	]
	edge [
		source 9
		target 32
		weight -0.552361786365509
	]
	edge [
		source 35
		target 38
		weight -0.670927107334137
	]
	edge [
		source 35
		target 39
		weight 0.698993444442749
	]
	edge [
		source 42
		target 45
		weight 0.8270142078399658
	]
	edge [
		source 41
		target 47
		weight 0.5610643625259399
	]
	edge [
		source 3
		target 49
		weight 0.4566185474395752
	]
	edge [
		source 9
		target 50
		weight 0.7377705574035645
	]
	edge [
		source 45
		target 51
		weight 0.5808349251747131
	]
	edge [
		source 2
		target 54
		weight 0.49799540638923645
	]
	edge [
		source 29
		target 56
		weight -0.4783535599708557
	]
	edge [
		source 44
		target 59
		weight -0.5052469968795776
	]
	edge [
		source 38
		target 60
		weight 0.5069976449012756
	]
	edge [
		source 14
		target 64
		weight 0.5827727913856506
	]
	edge [
		source 63
		target 65
		weight 0.7764759063720703
	]
	edge [
		source 13
		target 67
		weight 0.5408868193626404
	]
	edge [
		source 41
		target 67
		weight 0.3605307340621948
	]
	edge [
		source 42
		target 67
		weight -0.46762722730636597
	]
	edge [
		source 59
		target 67
		weight 0.3929583430290222
	]
	edge [
		source 62
		target 70
		weight -0.9999997615814209
	]
	edge [
		source 71
		target 72
		weight 0.0
	]
	edge [
		source 34
		target 74
		weight -0.6478942036628723
	]
	edge [
		source 56
		target 74
		weight -0.4160167872905731
	]
	edge [
		source 53
		target 75
		weight -0.9815412163734436
	]
	edge [
		source 55
		target 75
		weight -0.4920785427093506
	]
	edge [
		source 59
		target 75
		weight 0.5541889667510986
	]
	edge [
		source 3
		target 76
		weight 0.9999995827674866
	]
	edge [
		source 50
		target 76
		weight 0.7390997409820557
	]
	edge [
		source 24
		target 77
		weight 0.6998141407966614
	]
	edge [
		source 33
		target 77
		weight 0.5505402684211731
	]
	edge [
		source 50
		target 79
		weight 0.0
	]
	edge [
		source 42
		target 81
		weight 0.8993638753890991
	]
	edge [
		source 45
		target 81
		weight -0.9891491532325745
	]
	edge [
		source 80
		target 82
		weight -0.3607415556907654
	]
	edge [
		source 81
		target 82
		weight 0.6718806028366089
	]
	edge [
		source 50
		target 83
		weight 0.9999999403953552
	]
	edge [
		source 69
		target 84
		weight -0.5762678384780884
	]
	edge [
		source 30
		target 86
		weight 1.0
	]
	edge [
		source 38
		target 86
		weight -0.3970295608655665
	]
	edge [
		source 64
		target 86
		weight 0.7900711297988892
	]
	edge [
		source 80
		target 86
		weight 0.36213770508766174
	]
	edge [
		source 49
		target 88
		weight 0.9158175587654114
	]
	edge [
		source 81
		target 89
		weight 1.0
	]
	edge [
		source 87
		target 90
		weight 0.5518868565559387
	]
	edge [
		source 30
		target 91
		weight -0.8503846526145935
	]
	edge [
		source 36
		target 91
		weight -0.8184018731117249
	]
	edge [
		source 52
		target 91
		weight -0.9859611988067627
	]
	edge [
		source 87
		target 91
		weight -0.4580041170120239
	]
	edge [
		source 62
		target 92
		weight -0.8039665818214417
	]
	edge [
		source 86
		target 93
		weight 1.0
	]
	edge [
		source 39
		target 95
		weight -0.7385628819465637
	]
	edge [
		source 74
		target 95
		weight 0.7065389156341553
	]
	edge [
		source 59
		target 96
		weight -0.516141414642334
	]
]
