graph [
	directed 0
	node [
		id 1
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Actinomycetales;f__Micrococcaceae;g__Rothia;s__mucilaginosa"
		mv 0
	]
	node [
		id 2
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Actinomycetales;f__Micrococcaceae;g__Rothia;s__nasimurium"
		mv 0
	]
	node [
		id 3
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__adolescentis"
		mv 0
	]
	node [
		id 4
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__animalis"
		mv 0
	]
	node [
		id 5
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__bifidum"
		mv 0
	]
	node [
		id 6
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__longum"
		mv 0
	]
	node [
		id 7
		label "Root;k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__pseudolongum"
		mv 0
	]
	node [
		id 8
		label "Root;k__Bacteria;p__Actinobacteria;c__Coriobacteriia;o__Coriobacteriales;f__Coriobacteriaceae;g__Collinsella;s__aerofaciens"
		mv 0
	]
	node [
		id 9
		label "Root;k__Bacteria;p__Actinobacteria;c__Coriobacteriia;o__Coriobacteriales;f__Coriobacteriaceae;g__Eggerthella;s__lenta"
		mv 0
	]
	node [
		id 10
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__acidifaciens"
		mv 0
	]
	node [
		id 11
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__barnesiae"
		mv 0
	]
	node [
		id 12
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__caccae"
		mv 0
	]
	node [
		id 13
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__coprophilus"
		mv 0
	]
	node [
		id 14
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__eggerthii"
		mv 0
	]
	node [
		id 15
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__fragilis"
		mv 0
	]
	node [
		id 16
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__ovatus"
		mv 0
	]
	node [
		id 17
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__plebeius"
		mv 0
	]
	node [
		id 18
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Bacteroidaceae;g__Bacteroides;s__uniformis"
		mv 0
	]
	node [
		id 19
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Porphyromonadaceae;g__Parabacteroides;s__distasonis"
		mv 0
	]
	node [
		id 20
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Porphyromonadaceae;g__Parabacteroides;s__gordonii"
		mv 0
	]
	node [
		id 21
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Prevotellaceae;g__Prevotella;s__copri"
		mv 0
	]
	node [
		id 22
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Prevotellaceae;g__Prevotella;s__melaninogenica"
		mv 0
	]
	node [
		id 23
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Prevotellaceae;g__Prevotella;s__nigrescens"
		mv 0
	]
	node [
		id 24
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Prevotellaceae;g__Prevotella;s__stercorea"
		mv 0
	]
	node [
		id 25
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Rikenellaceae;g__Alistipes;s__indistinctus"
		mv 0
	]
	node [
		id 26
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Rikenellaceae;g__Alistipes;s__massiliensis"
		mv 0
	]
	node [
		id 27
		label "Root;k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__[Paraprevotellaceae];g__[Prevotella];s__tannerae"
		mv 0
	]
	node [
		id 28
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Enterococcaceae;g__Enterococcus;s__casseliflavus"
		mv 0
	]
	node [
		id 29
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Lactobacillaceae;g__Lactobacillus;s__brevis"
		mv 0
	]
	node [
		id 30
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Lactobacillaceae;g__Lactobacillus;s__iners"
		mv 0
	]
	node [
		id 31
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Lactobacillaceae;g__Lactobacillus;s__plantarum"
		mv 0
	]
	node [
		id 32
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Lactobacillaceae;g__Lactobacillus;s__zeae"
		mv 0
	]
	node [
		id 33
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Leuconostocaceae;g__Leuconostoc;s__mesenteroides"
		mv 0
	]
	node [
		id 34
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Streptococcaceae;g__Lactococcus;s__garvieae"
		mv 0
	]
	node [
		id 35
		label "Root;k__Bacteria;p__Firmicutes;c__Bacilli;o__Lactobacillales;f__Streptococcaceae;g__Streptococcus;s__anginosus"
		mv 0
	]
	node [
		id 36
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__botulinum"
		mv 0
	]
	node [
		id 37
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__butyricum"
		mv 0
	]
	node [
		id 38
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__intestinale"
		mv 0
	]
	node [
		id 39
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__perfringens"
		mv 0
	]
	node [
		id 40
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Clostridiaceae;g__Clostridium;s__tyrobutyricum"
		mv 0
	]
	node [
		id 41
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Blautia;s__obeum"
		mv 0
	]
	node [
		id 42
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Blautia;s__producta"
		mv 0
	]
	node [
		id 43
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Coprococcus;s__eutactus"
		mv 0
	]
	node [
		id 44
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Dorea;s__formicigenerans"
		mv 0
	]
	node [
		id 45
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Roseburia;s__faecis"
		mv 0
	]
	node [
		id 46
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__Shuttleworthia;s__satelles"
		mv 0
	]
	node [
		id 47
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__[Ruminococcus];s__gnavus"
		mv 0
	]
	node [
		id 48
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Lachnospiraceae;g__[Ruminococcus];s__torques"
		mv 0
	]
	node [
		id 49
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Peptostreptococcaceae;g__Clostridium;s__metallolevans"
		mv 0
	]
	node [
		id 50
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Peptostreptococcaceae;g__[Clostridioides];s__difficile"
		mv 0
	]
	node [
		id 51
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Butyricicoccus;s__pullicaecorum"
		mv 0
	]
	node [
		id 52
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Clostridium;s__hungatei"
		mv 0
	]
	node [
		id 53
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Clostridium;s__methylpentosum"
		mv 0
	]
	node [
		id 54
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Faecalibacterium;s__prausnitzii"
		mv 0
	]
	node [
		id 55
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Ruminococcus;s__bromii"
		mv 0
	]
	node [
		id 56
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Ruminococcus;s__callidus"
		mv 0
	]
	node [
		id 57
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Ruminococcus;s__flavefaciens"
		mv 0
	]
	node [
		id 58
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Veillonellaceae;g__Megamonas;s__hypermegale"
		mv 0
	]
	node [
		id 59
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Veillonellaceae;g__Mitsuokella;s__multacida"
		mv 0
	]
	node [
		id 60
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Veillonellaceae;g__Veillonella;s__dispar"
		mv 0
	]
	node [
		id 61
		label "Root;k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Veillonellaceae;g__Veillonella;s__parvula"
		mv 0
	]
	node [
		id 62
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Bulleidia;s__moorei"
		mv 0
	]
	node [
		id 63
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Bulleidia;s__p-1630-c5"
		mv 0
	]
	node [
		id 64
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Clostridium;s__cocleatum"
		mv 0
	]
	node [
		id 65
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__Coprobacillus;s__cateniformis"
		mv 0
	]
	node [
		id 66
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__[Eubacterium];s__biforme"
		mv 0
	]
	node [
		id 67
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__[Eubacterium];s__cylindroides"
		mv 0
	]
	node [
		id 68
		label "Root;k__Bacteria;p__Firmicutes;c__Erysipelotrichi;o__Erysipelotrichales;f__Erysipelotrichaceae;g__[Eubacterium];s__dolichum"
		mv 0
	]
	node [
		id 69
		label "Root;k__Bacteria;p__Lentisphaerae;c__[Lentisphaeria];o__Victivallales;f__Victivallaceae;g__Victivallis;s__vadensis"
		mv 0
	]
	node [
		id 70
		label "Root;k__Bacteria;p__Proteobacteria;c__Alphaproteobacteria;o__Rickettsiales;f__mitochondria;g__Myristica;s__fragrans"
		mv 0
	]
	node [
		id 71
		label "Root;k__Bacteria;p__Proteobacteria;c__Alphaproteobacteria;o__Rickettsiales;f__mitochondria;g__Nelumbo;s__nucifera"
		mv 0
	]
	node [
		id 72
		label "Root;k__Bacteria;p__Proteobacteria;c__Alphaproteobacteria;o__Rickettsiales;f__mitochondria;g__Victoria;s__amazonica"
		mv 0
	]
	node [
		id 73
		label "Root;k__Bacteria;p__Proteobacteria;c__Betaproteobacteria;o__Burkholderiales;f__Oxalobacteraceae;g__Oxalobacter;s__formigenes"
		mv 0
	]
	node [
		id 74
		label "Root;k__Bacteria;p__Proteobacteria;c__Betaproteobacteria;o__Rhodocyclales;f__Rhodocyclaceae;g__Dechloromonas;s__fungiphilus"
		mv 0
	]
	node [
		id 75
		label "Root;k__Bacteria;p__Proteobacteria;c__Deltaproteobacteria;o__Desulfovibrionales;f__Desulfovibrionaceae;g__Desulfovibrio;s__D168"
		mv 0
	]
	node [
		id 76
		label "Root;k__Bacteria;p__Proteobacteria;c__Epsilonproteobacteria;o__Campylobacterales;f__Campylobacteraceae;g__Campylobacter;s__ureolyticus"
		mv 0
	]
	node [
		id 77
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Alteromonadales;f__Shewanellaceae;g__Shewanella;s__algae"
		mv 0
	]
	node [
		id 78
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Cronobacter;s__dublinensis"
		mv 0
	]
	node [
		id 79
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Escherichia;s__blattae"
		mv 0
	]
	node [
		id 80
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Escherichia;s__coli"
		mv 0
	]
	node [
		id 81
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Enterobacteriales;f__Enterobacteriaceae;g__Plesiomonas;s__shigelloides"
		mv 0
	]
	node [
		id 82
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Actinobacillus;s__delphinicola"
		mv 0
	]
	node [
		id 83
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Actinobacillus;s__parahaemolyticus"
		mv 0
	]
	node [
		id 84
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Aggregatibacter;s__pneumotropica"
		mv 0
	]
	node [
		id 85
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Aggregatibacter;s__segnis"
		mv 0
	]
	node [
		id 86
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pasteurellales;f__Pasteurellaceae;g__Haemophilus;s__parainfluenzae"
		mv 0
	]
	node [
		id 87
		label "Root;k__Bacteria;p__Proteobacteria;c__Gammaproteobacteria;o__Pseudomonadales;f__Moraxellaceae;g__Acinetobacter;s__lwoffii"
		mv 0
	]
	node [
		id 88
		label "Root;k__Bacteria;p__Synergistetes;c__Synergistia;o__Synergistales;f__Dethiosulfovibrionaceae;g__Pyramidobacter;s__piscolens"
		mv 0
	]
	node [
		id 89
		label "Root;k__Bacteria;p__Verrucomicrobia;c__Verrucomicrobiae;o__Verrucomicrobiales;f__Verrucomicrobiaceae;g__Akkermansia;s__muciniphila"
		mv 0
	]
	edge [
		source 3
		target 6
		weight 0.8890284299850464
	]
	edge [
		source 3
		target 7
		weight 0.8201378583908081
	]
	edge [
		source 14
		target 18
		weight 0.7150581479072571
	]
	edge [
		source 11
		target 22
		weight 0.5247366428375244
	]
	edge [
		source 23
		target 24
		weight 0.9697068929672241
	]
	edge [
		source 29
		target 31
		weight 0.8193510174751282
	]
	edge [
		source 13
		target 32
		weight 0.6656126379966736
	]
	edge [
		source 22
		target 32
		weight 0.6880627274513245
	]
	edge [
		source 27
		target 36
		weight 0.9919848442077637
	]
	edge [
		source 35
		target 36
		weight 0.9499305486679077
	]
	edge [
		source 32
		target 37
		weight 0.4708356261253357
	]
	edge [
		source 9
		target 47
		weight 0.36110544204711914
	]
	edge [
		source 28
		target 47
		weight 0.40060698986053467
	]
	edge [
		source 23
		target 51
		weight 0.9214810132980347
	]
	edge [
		source 24
		target 52
		weight -0.7445278763771057
	]
	edge [
		source 28
		target 54
		weight -0.46903079748153687
	]
	edge [
		source 44
		target 54
		weight 0.36295709013938904
	]
	edge [
		source 45
		target 54
		weight 0.5381729006767273
	]
	edge [
		source 46
		target 56
		weight 0.0
	]
	edge [
		source 39
		target 57
		weight 0.5409941673278809
	]
	edge [
		source 34
		target 58
		weight -1.0
	]
	edge [
		source 26
		target 59
		weight 0.7180384993553162
	]
	edge [
		source 34
		target 62
		weight -0.6054028272628784
	]
	edge [
		source 52
		target 63
		weight -0.7026899456977844
	]
	edge [
		source 9
		target 64
		weight 0.507153332233429
	]
	edge [
		source 28
		target 64
		weight 0.9044763475321487
	]
	edge [
		source 42
		target 64
		weight 0.39125385880470276
	]
	edge [
		source 50
		target 64
		weight 0.7233788967132568
	]
	edge [
		source 28
		target 65
		weight 0.46948950978985887
	]
	edge [
		source 30
		target 65
		weight 0.6541825532913208
	]
	edge [
		source 44
		target 65
		weight -0.4902549386024475
	]
	edge [
		source 61
		target 65
		weight 0.629962682723999
	]
	edge [
		source 59
		target 67
		weight 1.0
	]
	edge [
		source 42
		target 68
		weight 0.6051231622695923
	]
	edge [
		source 65
		target 68
		weight 0.5113968849182129
	]
	edge [
		source 23
		target 70
		weight 0.9999999403953552
	]
	edge [
		source 23
		target 71
		weight 0.6742634773254395
	]
	edge [
		source 37
		target 71
		weight 0.5907924771308899
	]
	edge [
		source 31
		target 73
		weight 0.6290013194084167
	]
	edge [
		source 20
		target 74
		weight 0.5156477689743042
	]
	edge [
		source 28
		target 78
		weight 0.9544722437858582
	]
	edge [
		source 76
		target 80
		weight 0.5976600050926208
	]
	edge [
		source 78
		target 80
		weight -0.8149108290672302
	]
	edge [
		source 79
		target 81
		weight 0.0
	]
	edge [
		source 1
		target 83
		weight 0.6465070247650146
	]
	edge [
		source 73
		target 84
		weight 0.5302786827087402
	]
	edge [
		source 28
		target 85
		weight 1.0
	]
	edge [
		source 83
		target 86
		weight 0.638809084892273
	]
	edge [
		source 4
		target 87
		weight 0.5058181285858154
	]
]
